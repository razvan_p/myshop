<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Twig\Environment;
/**
 * Description of AccessDeniedHandler
 *
 * @author razva
 */
class AccessDeniedHandler implements AccessDeniedHandlerInterface{
    
    private $templating;
    
    public function __construct(Environment $twig)
    { 
        $this->templating = $twig;
    }
    
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        
        $content="Page not found";
        $view ='security/404.html.twig';
        $parameters=['error' => ['messageKey'=>'page not found']];
        $view = $this->templating->render($view,$parameters);
        return  new Response($view, 404);
        #return new Response($content, 404);
    }
}
