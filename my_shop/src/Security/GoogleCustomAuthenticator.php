<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
//use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpClient\HttpClient;
use App\Repository\UsersRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Twig\Environment;
use App\Entity\Users;

class GoogleCustomAuthenticator extends AbstractGuardAuthenticator
{
    use TargetPathTrait;
    
    private $entityManager;
    private $userRepository;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $urlGenerator;
    private $templating;
    
    public function __construct(EntityManagerInterface $entityManager,
        UsersRepository $user_rep, CsrfTokenManagerInterface $csrfTokenManager,
        UserPasswordEncoderInterface $passwordEncoder, 
        UrlGeneratorInterface $urlGenerator, Environment $twig) 
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $user_rep;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->urlGenerator = $urlGenerator;
        $this->templating = $twig;
    }
    
    public function supports(Request $request)
    {
        $hasCode =  $request->query->has('code')? true : false;
        $hasState = $request->query->has('state')? true : false;
        $hasGoogle = $request->query->has('google')? true : false;
        
        return ($hasCode && $hasState && $hasGoogle);
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'code' => $request->query->get('code',''),
            'csrfToken' => $request->query->get('state',''),
            'clientId' => $_ENV['GOOGLE_CLIENT_ID'],
            'clientSecret' => $_ENV['GOOGLE_CLIENT_SECRET'],
            'redirectUrl' => $_ENV['GOOGLE_REDIRECT_URI'],
        ];
        return $credentials;
       
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrfToken']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
           
            throw new InvalidCsrfTokenException();
        }
        $code = str_replace('%','/', $credentials['code']);

        $body = [
            'client_id' => $credentials['clientId'],
            'client_secret' => $credentials['clientSecret'],
            'grant_type' => 'authorization_code',
            'redirect_uri' => $credentials['redirectUrl'],
            'code' => $code,
        ];
        $headers = [
            'Host' => 'oauth2.googleapis.com',
            'Content-Type' => 'application/x-www-form-urlencoded',
            ];
        $accesCodeUrl = "https://oauth2.googleapis.com/token";

        $client = HttpClient::create();
//       geting the access token
        $response = $client->request('POST', $accesCodeUrl, 
            [
                'headers' => $headers,
                'body' => $body,
            ]);
        
        $statusCode = $response->getStatusCode();
        
        if ($statusCode != 200) {
//      error, error_description
            return null;
        }
        
        $content = $response->toArray();
        $accesToken =  $content['access_token'];
        $expiresIn =  $content['expires_in'];
              
        $emailUrl = "https://www.googleapis.com/oauth2/v1/userinfo?"
                ."access_token=$accesToken";
        
        $response1 = $client->request('GET', $emailUrl);
        $statusCode1 = $response1->getStatusCode();
        
        if ($statusCode != 200) {
            return null;
        }
        
        $content1 = $response1->toArray();
        $fullName = explode(' ',$content1['name']);
        $email = $content1['email'];
        $id = $content1['id'];
        //get user or add it in db
        $user = null;
        if ($statusCode1 == 200) {
            $user = $this->userRepository->findOneBy(['email' => $email,'isActive' => 1]);
        }
        
        if (!$user) {
            //check if email field is valid
            if (in_array($email,['','N/A',null])) {
                return null;
            } 
            
           $user = new Users();
           $user->setEmail($email);
           $user->setFirstName($fullName[1]);
           $user->setLastName($fullName[0]);
           $user->setGoogleAccessToken($accesToken);
           $user->setIsActive(true);
           $password = $this->passwordEncoder->encodePassword($user, $id);
           $user->setPassword($password);
           $user->setRoles(["ROLE_USER"]);
           $this->entityManager->persist($user);
           $this->entityManager->flush();
        } 
        else {
           $user->setGoogleAccessToken($accesToken);
           $this->entityManager->persist($user);
           $this->entityManager->flush();
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if (!$request->hasSession()) {
            throw new \LogicException('You can not use the addFlash method '
            . 'if sessions are disabled. Enable them in "config/packages/framework.yaml".');
        }
                
        $type = "ERROR";
        $message = "Ceva nu a mers cum trebuie, nu s-a realizat autentificarea";
        $request->getSession()->getFlashBag()->add($type, $message);
        return new RedirectResponse($this->urlGenerator->generate('user-login'));
//        for debug purpose
//        $content="Page not found";
//        $view ='security/404.html.twig';
//        $parameters=['error' => ['messageKey'=> $message]];
//        $view = $this->templating->render($view,$parameters);
//        return  new Response($view, 404);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
         if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }
        return new RedirectResponse($this->urlGenerator->generate('main_test'));
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        if (!$request->hasSession()) {
            throw new \LogicException('You can not use the addFlash method '
            . 'if sessions are disabled. Enable them in "config/packages/framework.yaml".');
        }
        $type = "ERROR";
        $message = "Datele utilizate pentru autentificare nu sunt corecte";
        $request->getSession()->getFlashBag()->add($type, $message);
        return new RedirectResponse($this->urlGenerator->generate('user-login'));
    }

    public function supportsRememberMe()
    {
       return false;
    }
}
