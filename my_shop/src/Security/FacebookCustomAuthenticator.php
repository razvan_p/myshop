<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
//use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpClient\HttpClient;
use App\Repository\UsersRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use App\Entity\Users;


class FacebookCustomAuthenticator extends AbstractGuardAuthenticator
{   
    use TargetPathTrait;
    
    private $entityManager;
    private $userRepository;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $urlGenerator;
    
    public function __construct(EntityManagerInterface $entityManager,
        UsersRepository $user_rep, CsrfTokenManagerInterface $csrfTokenManager,
        UserPasswordEncoderInterface $passwordEncoder, UrlGeneratorInterface $urlGenerator) 
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $user_rep;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->urlGenerator = $urlGenerator;
    }
    
    public function supports(Request $request)
    {
        $hasCode =  $request->query->has('code')? true : false;
        $hasState = $request->query->has('state')? true : false;
        $hasGoogle = $request->query->has('google')? true : false;
        
        return ($hasCode && $hasState) && !$hasGoogle ;
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'code' => $request->query->get('code',''),
            'csrfToken' => $request->query->get('state',''),
        ];
//        $request->getSession()->set(
//            Security::LAST_USERNAME,
//            $credentials['email']
//        );
        
        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        //transforms fb code in to access token
        $token = new CsrfToken('authenticate', $credentials['csrfToken']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
           
            throw new InvalidCsrfTokenException();
        }
        
        $fbAppId = $_ENV['FB_APP_ID'];
        $redirectUrl = $_ENV['FB_REDIRECT_URL'];
        $fbAppSecret = $_ENV['FB_TOKEN'];
        $query = [
            'client_id' => $fbAppId,'redirect_uri' => $redirectUrl,
            'client_secret' => $fbAppSecret, 'code' => $credentials['code']
            ];
                
        $accesCodeUrl = "https://graph.facebook.com/v7.0/oauth/access_token";
            //."client_id=$fbAppId&redirect_uri=$redirectUrl&"
            //."client_secret=$fbAppSecret&code=".$credentials['code'];
                
        $client = HttpClient::create();
        //geting the access token
        $response = $client->request('GET', $accesCodeUrl,['query' => $query]);
        $statusCode = $response->getStatusCode();
        
        if ($statusCode != 200) {
            return null;
        }
        
        $content = $response->toArray();
        $accesToken =  $content['access_token'];
        $expiresIn =  $content['expires_in'];
        $query_user = [
            'fields' => 'id,name,email',
            'access_token' => $accesToken
            ];
        //geting the email
        $emailUrl = "https://graph.facebook.com/me"; 
        //?fields=id,name,email&"
        //. "access_token=$accesToken";
        $response1 = $client->request('GET', $emailUrl,['query' => $query_user]);
        $statusCode1 = $response1->getStatusCode();
        
        if ($statusCode != 200) {
            return null;
        }
        
        $content1 = $response1->toArray();
        $fullName = explode(' ',$content1['name']);
        $email = $content1['email'];
        $id = $content1['id'];
        //get user or add it in db
        $user = null;
        if ($statusCode1 == 200) {
            $user = $this->userRepository->findOneBy(['email' => $email,'isActive' => 1]);
        }
       
        if (!$user) {
            //check if email field is valid
            if (in_array($email,['','N/A',null])) {
                return null;
            } 
            
           $user = new Users();
           $user->setEmail($email);
           $user->setFirstName($fullName[1]);
           $user->setLastName($fullName[0]);
           $user->setFacebookAccessToken($accesToken);
           $user->setIsActive(true);
           $password = $this->passwordEncoder->encodePassword($user, $id);
           $user->setPassword($password);
           $user->setRoles(["ROLE_USER"]);
           $this->entityManager->persist($user);
           $this->entityManager->flush();
        } 
        else {
           $user->setFacebookAccessToken($accesToken);
           $this->entityManager->persist($user);
           $this->entityManager->flush();
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if (!$request->hasSession()) {
            throw new \LogicException('You can not use the addFlash method '
            . 'if sessions are disabled. Enable them in "config/packages/framework.yaml".');
        }
        $type = "ERROR";
        $message = "Ceva nu a mers cum trebuie, nu s-a realizat autentificarea";
        $request->getSession()->getFlashBag()->add($type, $message);
        return new RedirectResponse($this->urlGenerator->generate('user-login'));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }
        return new RedirectResponse($this->urlGenerator->generate('main_test'));
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
       if (!$request->hasSession()) {
            throw new \LogicException('You can not use the addFlash method '
            . 'if sessions are disabled. Enable them in "config/packages/framework.yaml".');
        }
        $type = "ERROR";
        $message = "Datele utilizate pentru autentificare nu sunt corecte";
        $request->getSession()->getFlashBag()->add($type, $message);
        return new RedirectResponse($this->urlGenerator->generate('user-login'));
    }

    public function supportsRememberMe()
    {
         return false;
    }
}
