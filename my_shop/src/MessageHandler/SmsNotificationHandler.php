<?php
namespace App\MessageHandler;

use App\Message\SmsNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Description of SmsNotificationHandler
 *
 * @author razva
 */
class SmsNotificationHandler  implements MessageHandlerInterface{
    
    public function __invoke(SmsNotification $message) {
        // ... do some work - like sending an SMS message!
    }
}
