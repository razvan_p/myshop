<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Admins;

/**
 * Products
 *
 * @ORM\Table(name="products", indexes={@ORM\Index(name="FK__admins", columns={"created_by"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProductsRepository")
 */
class Products
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false, options={"default"="'introdu nume produs'"})
     */
    private $name = '\'introdu nume produs\'';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=false, options={"default"="'descriere produs'"})
     */
    private $description = '\'descriere produs\'';

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admins" ,cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id" ,nullable=true ,onDelete="SET NULL")
     */
    private $createdBy = 'NULL';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedBy(): ?Admins
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?Admins $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }


}
