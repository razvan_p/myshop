<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
//use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 * @UniqueEntity(fields="email", message="must be unique")
 */
class Users implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=50, nullable=false, options={"default"="''"})
     */
    private $firstName = '\'\'';

    /**
     * @var string
     
     * @ORM\Column(name="last_name", type="string", length=50, nullable=false, options={"default"="''"})
     */
    private $lastName = '\'\'';

    /**
     * @var string
     * @Assert\NotBlank(message = "please enter a valid password")
     * @ORM\Column(name="password", type="string", length=150, nullable=false, options={"default"="NULL"})
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_access_token", type="string", length=300, nullable=false, options={"default"="''"})
     */
    private $facebookAccessToken = '\'\'';
    
    /**
     * @var string
     *
     * @ORM\Column(name="google_access_token", type="string", length=300, nullable=false, options={"default"="''"})
     */
    private $googleAccessToken = '\'\'';
    
    /**
     * @var string
     *
     * @ORM\Column(name="google_refresh_token", type="string", length=300, nullable=false, options={"default"="''"})
     */
    private $googleRefreshToken = '\'\'';

    /**
    * @ORM\Column(type="json")
    */
    private $roles = [];
    
     /**
     * @var string|null
     *
     * @ORM\Column(name="email_confirmation_code", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $emailConfirmationCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password_reset_code", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $passwordResetCode;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = 0;
    
    public function getPasswordResetCode():?string
    {
        return $this->passwordResetCode;
    }
    
    public function setPasswordResetCode(?string $passwordcode): self
    {
         $this->passwordResetCode = $passwordcode;
         return $this;
    }
    
    public function setEmailConfirmationCode(?string $code): self
    {
        $this->emailConfirmationCode = $code;
        return $this;
    }
    
    public function getEmailConfirmationCode(): ?string
    {
        return $this->emailConfirmationCode;
    }
    
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFacebookAccessToken(): ?string
    {
        return $this->facebookAccessToken;
    }

    public function setFacebookAccessToken(string $facebookAccessToken): self
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }
    
    public function getGoogleAccessToken(): ?string
    {
        return $this->googleAccessToken;
    }

    public function setGoogleAccessToken(string $googleAccessToken): self
    {
        $this->googleAccessToken = $googleAccessToken;

        return $this;
    }
    
    public function getGoogleRefreshToken(): ?string
    {
        return $this->googleRefreshToken;
    }

    public function setGoogleRefreshToken(string $googleRefreshToken): self
    {
        $this->googleAccessToken = $googleRefreshToken;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

}
