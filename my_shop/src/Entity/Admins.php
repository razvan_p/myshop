<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Admins
 *
 * @ORM\Table(name="admins", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity(repositoryClass="App\Repository\AdminsRepository")
 * @UniqueEntity(fields="email", message="must be unique")
 */
class Admins implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string The hashed password
     *
     * @ORM\Column(name="full_name", type="string", length=150, nullable=false, options={"default"="\'\'"} )
     */
    private $fullName= '' ;

    /**
     * @var string
     * @Assert\NotBlank(message = "please enter a valid password")
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted = 0;

    /**
     * @var string A "Y-m-d H:i:s" formatted value
     * @ORM\Column(name="created_at", type="datetime", nullable=false ,options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var string A "Y-m-d H:i:s" formatted value
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt = NULL;

    /**
     * @var string|null
     *
     * @ORM\Column(name="confirmation_code", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $confirmationCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password_reste_code", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $passwordResteCode;
    
    /**
    * @ORM\Column(type="json")
    */
    private $roles = [];
    
//    private $products;
    
    public function __construct()
    {
//        $this->$products = new ArrayCollection();
    }
    
    /**
     * @return Collection|Products[]
     */
//    public function getProducts(): Collection
//    {
//        return $this->products;
//    }
//
//    public function addProduct(Products $product): self
//    {
//        if (!$this->products->contains($product)) {
//            $this->products[] = $product;
//            $product->setCreatedBy($this);
//        }
//
//        return $this;
//    }

//    public function removeProduct(Products $product): self
//    {
//        if ($this->products->contains($product)) {
//            $this->products->removeElement($product);
//            // set the owning side to null (unless already changed)
//            if ($product->getCreatedBy() === $this) {
//                $product->setCreatedBy(null);
//            }
//        }
//
//        return $this;
//    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFulName(): ?string
    {
        return $this->fullName;
    }

    public function setFulName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeinterface $createdAt): self
    {
        $this->createdAt = $createdAt ;#->format('Y-m-d H:mm:ss');
//        exit( print_r($this->createdAt));
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getConfirmationCode(): ?string
    {
        return $this->confirmationCode;
    }

    public function setConfirmationCode(?string $confirmationCode): self
    {
        $this->confirmationCode = $confirmationCode;

        return $this;
    }

    public function getPasswordResteCode(): ?string
    {
        return $this->passwordResteCode;
    }

    public function setPasswordResteCode(?string $passwordResteCode): self
    {
        $this->passwordResteCode = $passwordResteCode;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

}
