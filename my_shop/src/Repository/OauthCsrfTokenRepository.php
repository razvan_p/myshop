<?php

namespace App\Repository;

use App\Entity\OauthCsrfToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OauthCsrfToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method OauthCsrfToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method OauthCsrfToken[]    findAll()
 * @method OauthCsrfToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OauthCsrfTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OauthCsrfToken::class);
    }

    // /**
    //  * @return OauthCsrfToken[] Returns an array of OauthCsrfToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function findOneByToken($value, $date): ?OauthCsrfToken
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.token = :val')
            ->setParameter(['val', $value])
            ->andWhere('o.date > :date')
            ->setParameter(['val', $date])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
