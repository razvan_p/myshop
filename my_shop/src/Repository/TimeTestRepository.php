<?php

namespace App\Repository;

use App\Entity\TimeTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TimeTest|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeTest|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeTest[]    findAll()
 * @method TimeTest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeTestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeTest::class);
    }

    // /**
    //  * @return TimeTest[] Returns an array of TimeTest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TimeTest
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
