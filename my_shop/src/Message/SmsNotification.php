<?php
namespace App\Message;

class SmsNotification {
    private $content;
    function __construct(string $content) {
        $this->content = $content;
    }
    public function getContent(): string {
        return $this->content;
    }
}
