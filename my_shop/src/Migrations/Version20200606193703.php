<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200606193703 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE oauth_csrf_token (id INT AUTO_INCREMENT NOT NULL, date DATETIME NOT NULL, token VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE time_test (id INT AUTO_INCREMENT NOT NULL, time DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE admins CHANGE full_name full_name VARCHAR(150) DEFAULT \'\\\\\'\'\\\\\'\'\' NOT NULL, CHANGE password password VARCHAR(255) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL, CHANGE is_active is_active TINYINT(1) NOT NULL, CHANGE is_deleted is_deleted TINYINT(1) NOT NULL, CHANGE created_at created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE products CHANGE deleted deleted TINYINT(1) NOT NULL, CHANGE created_by created_by INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE products RENAME INDEX fk_products_admins TO FK__admins');
        $this->addSql('ALTER TABLE users CHANGE password password VARCHAR(150) DEFAULT \'NULL\' NOT NULL, CHANGE roles roles JSON NOT NULL, CHANGE is_active is_active TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE oauth_csrf_token');
        $this->addSql('DROP TABLE time_test');
        $this->addSql('ALTER TABLE admins CHANGE full_name full_name VARCHAR(150) CHARACTER SET utf8mb4 DEFAULT \'\'\'\'\'\' NOT NULL COLLATE `utf8mb4_general_ci`, CHANGE password password VARCHAR(255) CHARACTER SET latin7 DEFAULT \'\'\'\'\'\' NOT NULL COLLATE `latin7_general_ci`, CHANGE email email VARCHAR(255) CHARACTER SET utf8 DEFAULT \'\'\'\'\'\' NOT NULL COLLATE `utf8_general_ci`, CHANGE is_active is_active TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_deleted is_deleted TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE created_at created_at DATETIME NOT NULL, CHANGE roles roles VARCHAR(255) CHARACTER SET latin1 DEFAULT \'NULL\' COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5ADE12AB56');
        $this->addSql('ALTER TABLE products CHANGE created_by created_by INT UNSIGNED DEFAULT NULL, CHANGE deleted deleted TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE products RENAME INDEX fk__admins TO FK_products_admins');
        $this->addSql('ALTER TABLE users CHANGE password password VARCHAR(150) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE roles roles VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'\'\'\'\'\' NOT NULL COLLATE `utf8mb4_general_ci`, CHANGE is_active is_active TINYINT(1) DEFAULT \'0\' NOT NULL');
    }
}
