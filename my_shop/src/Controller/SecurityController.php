<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\Users;
use App\Entity\Admins;
use App\Form\UserRegisterFormType;
use App\Form\AdminRegisterType;
use DateTime;

/**
* Route("/auth")
*/
class SecurityController extends AbstractController
{
    /**
     * @Route("/login/", name="user-login" , methods={"POST","GET"})
     */
    public function userLogin(Request $request, AuthenticationUtils $auth)
    {
        $error = $auth->getLastAuthenticationError();
        $last_user = $auth->getLastUsername();
        // dump($_ENV);
        $fb_app_id = $_ENV['FB_APP_ID'];
        $redirect_url = $_ENV['FB_REDIRECT_URL'];
        $googleRedirectUrl = $_ENV['GOOGLE_REDIRECT_URI'];
        $googleId = $_ENV['GOOGLE_CLIENT_ID'];
        // $googleId = $_ENV['GOOGLE_EMAIL'];
        
        $csrf = $this->container->get('security.csrf.token_manager');
        $intention = 'authenticate';
        $token = $csrf->getToken($intention)->getValue();
        $googleLoginUrl = "https://accounts.google.com/o/oauth2/v2/auth?"
                ."client_id=$googleId&redirect_uri=$googleRedirectUrl&"
                ."response_type=code&"
                ."scope=https://www.googleapis.com/auth/userinfo.email "
                ."https://www.googleapis.com/auth/userinfo.profile&"
                //."scope=profile email&"
                ."state=$token";
        
        $fbLoginUrl = "https://www.facebook.com/v7.0/dialog/oauth?"
                ."client_id=$fb_app_id&"
                ."redirect_uri=$redirect_url&"
                ."state=$token";

        return $this->render('security/login_user.html.twig', [
            'error'  => $error,
            'last_user_name' => $last_user,
            'fb_url' => $fbLoginUrl,
            'google_url' => $googleLoginUrl,
        ]);
    }
    
    /**
     * @Route("/admin/login", name="admin-login" , methods={"POST","GET"})
     */
    public function adminLogin(Request $request, AuthenticationUtils $auth)
    {
        $error = $auth->getLastAuthenticationError();
        $last_user = $auth->getLastUsername();
        
        return $this->render('security/login_admin.html.twig', [
            'error'  => $error,
            'last_user_name' => $last_user
            
        ]);
    }
    
    /**
     * @Route("/register/user", name="user-register")
     */
     public function createUser(Request $request, UserPasswordEncoderInterface $passEncoder) {
        $entitymanager = $this->getDoctrine()->getManager();
        $user = new Users();
        $form = $this->createForm(UserRegisterFormType::class, $user);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $user->setPassword($passEncoder->encodePassword($user, $form->get('password')->getData()));
            $user->setRoles(['ROLE_USER']);
            $user->setEmail($form->get('email')->getData());
            $entitymanager->persist($user);
            $entitymanager->flush();
            return $this->redirectToRoute('main_test');
        }
       
        $message = "add new user";
        $res =  $this->render('security/register.html.twig',[
            
            'message'=>$message,
            'form'=> $form->createView(),
        ]);
        return $res; 
   }
   /**
     * @Route("/register/admin", name="admin-register")
     */
     public function createAdmin(Request $request, UserPasswordEncoderInterface $passEncoder) {
        $entitymanager = $this->getDoctrine()->getManager();
        $user = new Admins();
        $form = $this->createForm(AdminRegisterType::class, $user);
        $form->handleRequest($request);
        $date = new DateTime();
//        exit($date->format('Y-m-d H:i:s'));
        if($form->isSubmitted() && $form->isValid()){
            $user->setPassword($passEncoder->encodePassword($user, $form->get('password')->getData()));
            $user->setRoles(['ROLE_SUPER_ADMIN']);
            $user->setEmail($form->get('email')->getData());
            $user->setCreatedAt($date); # ->format('Y-m-d H:i:s')
            $entitymanager->persist($user);
            $entitymanager->flush();
            return $this->redirectToRoute('main_admin');
        }
       
        $message = "add new admin";
        $res =  $this->render('security/register.html.twig',[
            
            'message'=>$message,
            'form'=> $form->createView(),
        ]);
        return $res; 
   }
   
    /**
    * @Route("/logout", name="app_logout", methods={"GET"})
    */
    public function logout()
    {
        // controller can be blank: it will never be executed!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}
