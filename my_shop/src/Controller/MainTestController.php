<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security; // pentru anotati

use App\Entity\Products;

class MainTestController extends AbstractController
{
    /**
     * @Route("/", name="main_test")
     * adding role to be log in to add new
     * @Security("is_granted('ROLE_USER')")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $prod = $this->getDoctrine()->getRepository(Products::class)->findAll();
        return $this->render('main_test/index.html.twig', [
            'controller_name' => 'MainTestController',
            'products' => $prod
        ]);
    }
    
     /**
     * @Route("/admin", name="main_admin")
     * adding role to be log in to add new
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function admin()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $prod = $this->getDoctrine()->getRepository(Products::class)->findAll();
        return $this->render('main_test/index.html.twig', [
            'controller_name' => 'MainTestController',
            'products' => $prod
        ]);
    }
}
